package com.example.searchtest.data.model


import com.google.gson.annotations.SerializedName

data class DataHashTag(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("status")
    val status: Status
) {
    data class Data(
        @SerializedName("count")
        val count: Any,
        @SerializedName("countByCountry")
        val countByCountry: String,
        @SerializedName("country")
        val country: Any,
        @SerializedName("createdDate")
        val createdDate: Long,
        @SerializedName("description")
        val description: Any,
        @SerializedName("displayByLang")
        val displayByLang: String,
        @SerializedName("hashtag")
        val hashtag: String,
        @SerializedName("id")
        val id: Long,
        @SerializedName("isMin")
        val isMin: Boolean,
        @SerializedName("isShow")
        val isShow: Boolean,
        @SerializedName("isVideo")
        val isVideo: Any,
        @SerializedName("name")
        val name: String,
        @SerializedName("searchName")
        val searchName: Any,
        @SerializedName("url")
        val url: Any
    )

    data class Status(
        @SerializedName("message")
        val message: String,
        @SerializedName("statusCode")
        val statusCode: Int
    )
}