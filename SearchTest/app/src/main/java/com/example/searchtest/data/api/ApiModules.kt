package com.example.searchtest.data.api

import com.example.searchtest.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModules {
    var BASE_URL = "https://ringdev.alo.vn"


    @Singleton
    @Provides
    fun provideRetrofit() : Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .build()

//    fun create() : ApiServices {
//        val retrofit = Retrofit.Builder()
//            .addConverterFactory(GsonConverterFactory.create())
//            .baseUrl(BASE_URL)
//            .build()
//        return retrofit.create(ApiServices::class.java)
//    }

    @Singleton
    @Provides
    fun provideApiServices(retrofit: Retrofit) : ApiServices = retrofit.create(ApiServices::class.java)

    @Singleton
    @Provides
    fun provideRepository(apiServices: ApiServices) = Repository(apiServices)
}