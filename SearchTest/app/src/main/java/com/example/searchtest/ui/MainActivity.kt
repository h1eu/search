package com.example.searchtest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.example.searchtest.R
import com.example.searchtest.databinding.ActivityMainBinding
import com.example.searchtest.ui.fragment.ExploreFragment
import com.example.searchtest.ui.fragment.HomeFragment
import com.example.searchtest.ui.fragment.SearchFragment
import com.example.searchtest.ui.fragment.UserFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpBottomNav()


    }

    private fun setUpBottomNav() {
        supportFragmentManager.beginTransaction().replace(R.id.frame_container, HomeFragment()).commit()
        binding.botNav.setOnItemSelectedListener(NavigationBarView.OnItemSelectedListener {
            when(it.itemId){
                R.id.home -> {
                    var fragment : HomeFragment = HomeFragment()
                    addFragment(fragment)
                    return@OnItemSelectedListener true
                }
                R.id.explore -> {
                    var fragment : ExploreFragment = ExploreFragment()
                    addFragment(fragment)
                    return@OnItemSelectedListener true
                }
                R.id.search -> {
                    var fragment : SearchFragment = SearchFragment()
                    addFragment(fragment)
                    return@OnItemSelectedListener true
                }
                R.id.user -> {
                    var fragment : UserFragment = UserFragment()
                    addFragment(fragment)
                    return@OnItemSelectedListener true
                }
            }
            return@OnItemSelectedListener false
        })
    }

    private fun addFragment(fragment : Fragment){
        var fragmentTransaction : FragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container,fragment)
        fragmentTransaction.commit()
    }
}

