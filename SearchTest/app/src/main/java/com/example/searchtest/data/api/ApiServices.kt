package com.example.searchtest.data.api

import com.example.searchtest.data.model.DataHashTag
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiServices {
    @GET("/api/hashtag")
    suspend fun getHashTag(
        @Header("language") language: String,
        @Header("country") country : String,
        @Header("X-AppVersion") X_AppVersion: String,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ) : Response<DataHashTag>
}