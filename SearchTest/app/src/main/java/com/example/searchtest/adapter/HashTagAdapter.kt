package com.example.searchtest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.searchtest.databinding.RowItemBinding
import com.example.searchtest.data.model.DataHashTag
import org.greenrobot.eventbus.EventBus

class HashTagAdapter(listHashTag : ArrayList<DataHashTag.Data>) : RecyclerView.Adapter<HashTagAdapter.HashTagViewHolder>() {
    private var list = listHashTag
    inner class HashTagViewHolder(binding : RowItemBinding) : RecyclerView.ViewHolder(binding.root){
        var bd : RowItemBinding
        init {
            bd = binding
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HashTagViewHolder {
        val inflate = LayoutInflater.from(parent.context)
        val binding = RowItemBinding.inflate(inflate,parent,false)
        return HashTagViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HashTagViewHolder, position: Int) {
        var ht = list[position]
        holder.bd.tvHashTag.text = ht.hashtag
        holder.bd.tvHashTag.setOnClickListener(View.OnClickListener {
            var x = holder.bd.tvHashTag.text
            var y = list[position]
            EventBus.getDefault().post(holder.bd.tvHashTag.text)

        })

    }

    override fun getItemCount(): Int {
        return list.size
    }
}