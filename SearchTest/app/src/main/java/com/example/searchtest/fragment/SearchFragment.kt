package com.example.searchtest.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.searchtest.adapter.HashTagAdapter
import com.example.searchtest.data.api.ApiServices
import com.example.searchtest.databinding.FragmentSearchBinding
import com.example.searchtest.constant.Constant
import com.example.searchtest.data.model.DataHashTag
import com.example.searchtest.data.api.viewmodel.SearchViewModel
import com.example.searchtest.repository.Repository
import com.example.searchtest.viewmodel.SearchViewModelFractory
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SearchFragment : Fragment() {
    private lateinit var binding: FragmentSearchBinding
    private lateinit var mSearchHashTag : SearchViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchBinding.inflate(layoutInflater, container, false)
        mSearchHashTag = ViewModelProvider(requireActivity(),SearchViewModelFractory(Repository(
            ApiServices.create())))[SearchViewModel::class.java]
        setUpUI()
        return binding.root
    }





    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    private fun setUpUI() {
        mSearchHashTag.getHashTag()
        mSearchHashTag.listHashTag.observe(requireActivity(), Observer {
            binding.setup()
        })

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public fun onMsgEvent(msg : DataHashTag.Data){
        Log.e("event",msg.name)
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }


    private fun FragmentSearchBinding.setup(){
        val adapter = HashTagAdapter(mSearchHashTag.listHashTag.value!!)
        val flexBoxLayoutManager = FlexboxLayoutManager(requireActivity())
        flexBoxLayoutManager.flexWrap = FlexWrap.WRAP
        flexBoxLayoutManager.flexDirection = FlexDirection.ROW
        binding.searchFRecyclerView.layoutManager = flexBoxLayoutManager
        binding.searchFRecyclerView.adapter = adapter
    }


}