package com.example.searchtest.ui.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.searchtest.R
import com.example.searchtest.databinding.RowItemBinding
import com.example.searchtest.data.model.DataHashTag
import com.example.searchtest.ui.`interface`.ClickItemHashTag
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

class HashTagAdapter @Inject constructor(context: Context) :
    RecyclerView.Adapter<HashTagAdapter.HashTagViewHolder>() {
    var list : List<DataHashTag.Data> = emptyList()
    private var mContext = context
    private lateinit var listener: ClickItemHashTag

    inner class HashTagViewHolder(binding: RowItemBinding, listener: ClickItemHashTag) :
        RecyclerView.ViewHolder(binding.root) {
        var bd: RowItemBinding

        init {
            bd = binding

        }

    }

    fun setOnClick(listener: ClickItemHashTag) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HashTagViewHolder {
        val inflate = LayoutInflater.from(parent.context)
        val binding = RowItemBinding.inflate(inflate, parent, false)
        return HashTagViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: HashTagViewHolder, position: Int) {
        var ht = list[position]
        holder.bd.tvHashTag.text = ht.hashtag
        holder.bd.tvHashTag.setOnClickListener(View.OnClickListener {
            listener.onItemHashTagClick(position)
        })



    }

    override fun getItemCount(): Int {
        return list.size
    }
}