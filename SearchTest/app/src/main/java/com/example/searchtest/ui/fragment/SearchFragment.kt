package com.example.searchtest.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.searchtest.ui.adapter.HashTagAdapter
import com.example.searchtest.data.api.ApiServices

import com.example.searchtest.data.api.viewmodel.SearchViewModel
import com.example.searchtest.data.model.DataHashTag
import com.example.searchtest.databinding.FragmentSearchBinding
import com.example.searchtest.repository.Repository
import com.example.searchtest.ui.`interface`.ClickItemHashTag
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

@AndroidEntryPoint
class SearchFragment : Fragment() {
    private lateinit var binding: FragmentSearchBinding
    private val mSearchHashTag : SearchViewModel by viewModels()
    private var list : List<DataHashTag.Data> = emptyList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchBinding.inflate(layoutInflater, container, false)
        binding.setup()
        setupObserver()
        return binding.root
    }





    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    private fun setupObserver(){
        mSearchHashTag.getHashTag()
        mSearchHashTag.listHashTag.observe(requireActivity()) { hashtagList ->
            hashtagList.let {
                binding.searchFRecyclerView.apply {
                    with(adapter as HashTagAdapter){
                        list = hashtagList
                        notifyDataSetChanged()
                        setOnClick(
                            object :ClickItemHashTag{
                                override fun onItemHashTagClick(postion: Int) {
                                    Log.e("click interface", mSearchHashTag.listHashTag.value!![postion].name.toString())
                                }
                            }
                        )
                    }
                }
            }
        }
    }




    private fun FragmentSearchBinding.setup(){
        val flexBoxLayoutManager = FlexboxLayoutManager(requireActivity())
        flexBoxLayoutManager.flexWrap = FlexWrap.WRAP
        flexBoxLayoutManager.flexDirection = FlexDirection.ROW
        binding.searchFRecyclerView.apply {
            layoutManager = flexBoxLayoutManager
            adapter = HashTagAdapter(requireContext())
        }


    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public fun onMsgEvent(msg : String){
        Log.e("event",msg)
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

}