package com.example.searchtest.repository

import com.example.searchtest.data.constant.Constant
import com.example.searchtest.data.api.ApiServices
import javax.inject.Inject

class Repository @Inject constructor(private val apiServices: ApiServices) {
    suspend fun getHashTag() = apiServices.getHashTag(
        Constant.KeyWithScreen.KEY_SEND_LANGUAGE,
        Constant.KeyWithScreen.KEY_SEND_COUNTRY,
        Constant.KeyWithScreen.KEY_SEND_X_APPVERSION,
        Constant.KeyWithScreen.KEY_SEND_OFFSET,
        Constant.KeyWithScreen.KEY_SEND_LIMIT)
}