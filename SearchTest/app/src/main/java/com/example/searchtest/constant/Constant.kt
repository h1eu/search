package com.example.searchtest.constant

class Constant {
    class KeyWithScreen{
        companion object{
            const val KEY_SEND_OFFSET = 0
            const val KEY_SEND_LIMIT = 20
            const val KEY_SEND_LANGUAGE = "vi"
            const val KEY_SEND_COUNTRY = "VN"
            const val KEY_SEND_X_APPVERSION = ""
        }
    }
}