package com.example.searchtest.data.api.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.searchtest.data.model.DataHashTag
import com.example.searchtest.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val repo : Repository) : ViewModel() {

    private val _listHashTag : MutableLiveData<ArrayList<DataHashTag.Data>> = MutableLiveData()


    val listHashTag = _listHashTag


     fun getHashTag() {
        viewModelScope.launch {
            val hashTag = repo.getHashTag()
            var listData : Deferred<ArrayList<DataHashTag.Data>> = async(Dispatchers.IO){
                var tmp = arrayListOf<DataHashTag.Data>()
                when(hashTag.isSuccessful){
                    true ->
                    {
                        tmp = hashTag.body()?.data as ArrayList<DataHashTag.Data>
                    }
                    else -> {

                    }
                }
                tmp
            }
            _listHashTag.postValue(listData.await())
        }

    }




}